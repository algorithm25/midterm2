/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nippon.midterm2;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author Nippon
 */
public class mid2 {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int[] arr = new int[5];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = kb.nextInt();
        }
        System.out.println("Output of the Algorithm :" + MaxsubFaster(arr));
    }

    private static int MaxsubFaster(int[] arr) {
        int sum[] = new int[arr.length];
        Arrays.fill(sum, 0);

        sum[0] = 0;
        for (int i = 1; i < arr.length; i++) {
            sum[i] = sum[i - 1] + arr[i];
        }
        int max = 0;
        for (int j = 1; j < arr.length; j++) {
            for (int k = j; k < arr.length; k++) {
                int subtotal = sum[k] - sum[j - 1];
                max = Math.max(max, subtotal);
            }
        }
        return max;
    }
}
